// UHRSwitch.ino - UhrSwitchLogic.ino
// @arduinoenigma 2020

// 500 is min
#define etimervalue 600

struct UHRData_t     /*xls*/
{
  byte UHR;
}
UHRData;


GPIO<BOARD::D26> Plug1A;
GPIO<BOARD::D28> Plug1B;

GPIO<BOARD::D31> Plug2A;
GPIO<BOARD::D30> Plug2B;

GPIO<BOARD::D27> Plug3A;
GPIO<BOARD::D29> Plug3B;

GPIO<BOARD::D23> Plug4A;
GPIO<BOARD::D24> Plug4B;

GPIO<BOARD::D19> Plug5A;
GPIO<BOARD::D25> Plug5B;

GPIO<BOARD::D11> Plug6A;
GPIO<BOARD::D22> Plug6B;

GPIO<BOARD::D9>  Plug7A;
GPIO<BOARD::D20> Plug7B;

GPIO<BOARD::D7>  Plug8A;
GPIO<BOARD::D21> Plug8B;

GPIO<BOARD::D15> Plug9A;
GPIO<BOARD::D18> Plug9B;

GPIO<BOARD::D17> Plug10A;
GPIO<BOARD::D16> Plug10B;


//
void allPlugsInput()     /*xls*/
{
  Plug1A.input();
  Plug2A.input();
  Plug3A.input();
  Plug4A.input();
  Plug5A.input();
  Plug6A.input();
  Plug7A.input();
  Plug8A.input();
  Plug9A.input();
  Plug10A.input();

  Plug1B.input();
  Plug2B.input();
  Plug3B.input();
  Plug4B.input();
  Plug5B.input();
  Plug6B.input();
  Plug7B.input();
  Plug8B.input();
  Plug9B.input();
  Plug10B.input();

  Plug1A.high();
  Plug2A.high();
  Plug3A.high();
  Plug4A.high();
  Plug5A.high();
  Plug6A.high();
  Plug7A.high();
  Plug8A.high();
  Plug9A.high();
  Plug10A.high();

  Plug1B.high();
  Plug2B.high();
  Plug3B.high();
  Plug4B.high();
  Plug5B.high();
  Plug6B.high();
  Plug7B.high();
  Plug8B.high();
  Plug9B.high();
  Plug10B.high();
}


//
byte readPlug(byte plug)     /*xls*/
{
  switch (plug)
  {
    case 0:
      {
        return Plug1A.read();  // no need for break, return exits function
      }
    case 1:
      {
        return Plug2A.read();
      }
    case 2:
      {
        return Plug3A.read();
      }
    case 3:
      {
        return Plug4A.read();
      }
    case 4:
      {
        return Plug5A.read();
      }
    case 5:
      {
        return Plug6A.read();
      }
    case 6:
      {
        return Plug7A.read();
      }
    case 7:
      {
        return Plug8A.read();
      }
    case 8:
      {
        return Plug9A.read();
      }
    case 9:
      {
        return Plug10A.read();
      }

    case 10:
      {
        return Plug1B.read();
      }
    case 11:
      {
        return Plug2B.read();
      }
    case 12:
      {
        return Plug3B.read();
      }
    case 13:
      {
        return Plug4B.read();
      }
    case 14:
      {
        return Plug5B.read();
      }
    case 15:
      {
        return Plug6B.read();
      }
    case 16:
      {
        return Plug7B.read();
      }
    case 17:
      {
        return Plug8B.read();
      }
    case 18:
      {
        return Plug9B.read();
      }
    case 19:
      {
        return Plug10B.read();
      }

    default:
      {
        return 1;
      }
  }
}


//
void writePlug(byte plug, byte pval)     /*xls*/
{
  switch (plug)
  {
    case 0:
      {
        if (pval)
        {
          Plug1A.input();
          Plug1A.high();
        }
        else
        {
          Plug1A.low();
          Plug1A.output();
        }
        break;
      }

    case 1:
      {
        if (pval)
        {
          Plug2A.input();
          Plug2A.high();
        }
        else
        {
          Plug2A.low();
          Plug2A.output();
        }
        break;
      }

    case 2:
      {
        if (pval)
        {
          Plug3A.input();
          Plug3A.high();
        }
        else
        {
          Plug3A.low();
          Plug3A.output();
        }
        break;
      }

    case 3:
      {
        if (pval)
        {
          Plug4A.input();
          Plug4A.high();
        }
        else
        {
          Plug4A.low();
          Plug4A.output();
        }
        break;
      }

    case 4:
      {
        if (pval)
        {
          Plug5A.input();
          Plug5A.high();
        }
        else
        {
          Plug5A.low();
          Plug5A.output();
        }
        break;
      }

    case 5:
      {
        if (pval)
        {
          Plug6A.input();
          Plug6A.high();
        }
        else
        {
          Plug6A.low();
          Plug6A.output();
        }
        break;
      }

    case 6:
      {
        if (pval)
        {
          Plug7A.input();
          Plug7A.high();
        }
        else
        {
          Plug7A.low();
          Plug7A.output();
        }
        break;
      }

    case 7:
      {
        if (pval)
        {
          Plug8A.input();
          Plug8A.high();
        }
        else
        {
          Plug8A.low();
          Plug8A.output();
        }
        break;
      }

    case 8:
      {
        if (pval)
        {
          Plug9A.input();
          Plug9A.high();
        }
        else
        {
          Plug9A.low();
          Plug9A.output();
        }
        break;
      }

    case 9:
      {
        if (pval)
        {
          Plug10A.input();
          Plug10A.high();
        }
        else
        {
          Plug10A.low();
          Plug10A.output();
        }
        break;
      }

    case 10:
      {
        if (pval)
        {
          Plug1B.input();
          Plug1B.high();
        }
        else
        {
          Plug1B.low();
          Plug1B.output();
        }
        break;
      }

    case 11:
      {
        if (pval)
        {
          Plug2B.input();
          Plug2B.high();
        }
        else
        {
          Plug2B.low();
          Plug2B.output();
        }
        break;
      }

    case 12:
      {
        if (pval)
        {
          Plug3B.input();
          Plug3B.high();
        }
        else
        {
          Plug3B.low();
          Plug3B.output();
        }
        break;
      }

    case 13:
      {
        if (pval)
        {
          Plug4B.input();
          Plug4B.high();
        }
        else
        {
          Plug4B.low();
          Plug4B.output();
        }
        break;
      }

    case 14:
      {
        if (pval)
        {
          Plug5B.input();
          Plug5B.high();
        }
        else
        {
          Plug5B.low();
          Plug5B.output();
        }
        break;
      }

    case 15:
      {
        if (pval)
        {
          Plug6B.input();
          Plug6B.high();
        }
        else
        {
          Plug6B.low();
          Plug6B.output();
        }
        break;
      }

    case 16:
      {
        if (pval)
        {
          Plug7B.input();
          Plug7B.high();
        }
        else
        {
          Plug7B.low();
          Plug7B.output();
        }
        break;
      }

    case 17:
      {
        if (pval)
        {
          Plug8B.input();
          Plug8B.high();
        }
        else
        {
          Plug8B.low();
          Plug8B.output();
        }
        break;
      }

    case 18:
      {
        if (pval)
        {
          Plug9B.input();
          Plug9B.high();
        }
        else
        {
          Plug9B.low();
          Plug9B.output();
        }
        break;
      }

    case 19:
      {
        if (pval)
        {
          Plug10B.input();
          Plug10B.high();
        }
        else
        {
          Plug10B.low();
          Plug10B.output();
        }
        break;
      }
  }
}


//
void SetUHR(byte currentuhr)     /*xls*/
{
  UHRData.UHR = currentuhr;
}


//
byte GetPlug(byte uhr, byte plug, byte TopBot)      /*xls*/
{
  const __FlashStringHelper *UHRTopBot = F("\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0C\x0D\x12\x11\x0E\x10\x0A\x13\x0B\x0F\x07\x05\x00\x03\x01\x08\x02\x09\x04\x06\x10\x0F\x0E\x13\x12\x11\x0D\x0A\x0B\x0C\x09\x00\x01\x02\x03\x04\x05\x06\x07\x08\x0E\x12\x0D\x13\x0B\x0F\x0C\x10\x11\x0A\x05\x07\x09\x00\x03\x08\x04\x02\x01\x06\x0F\x0E\x13\x12\x11\x0D\x0A\x0B\x0C\x10\x06\x07\x08\x05\x01\x00\x09\x04\x03\x02\x13\x0B\x0A\x12\x0D\x10\x0C\x0F\x11\x0E\x08\x03\x05\x01\x09\x04\x06\x07\x00\x02\x11\x12\x0C\x0B\x0A\x13\x10\x0F\x0E\x0D\x05\x06\x07\x04\x00\x09\x08\x03\x02\x01\x0B\x13\x0C\x0F\x11\x0E\x0D\x0A\x10\x12\x01\x00\x05\x03\x08\x06\x04\x07\x02\x09\x12\x0C\x0B\x0A\x13\x10\x0F\x0E\x0D\x11\x03\x02\x01\x08\x07\x06\x05\x09\x00\x04\x0F\x10\x0B\x13\x0D\x0E\x11\x0A\x12\x0C\x06\x09\x01\x05\x04\x02\x07\x03\x08\x00\x0B\x0E\x0F\x10\x0C\x0D\x11\x12\x13\x0A\x02\x01\x00\x07\x06\x05\x04\x08\x09\x03\x0C\x0E\x11\x0A\x12\x13\x10\x0D\x0B\x0F\x06\x01\x08\x03\x04\x09\x00\x05\x07\x02\x0E\x0F\x10\x0C\x0D\x11\x12\x13\x0A\x0B\x08\x09\x03\x04\x00\x01\x02\x05\x06\x07\x0D\x0F\x0C\x13\x12\x0A\x10\x0B\x0E\x11\x02\x07\x09\x06\x00\x08\x05\x01\x03\x04\x12\x11\x0D\x0E\x13\x0A\x0B\x0C\x10\x0F\x07\x08\x02\x03\x09\x00\x01\x04\x05\x06\x12\x0A\x10\x0B\x0C\x0D\x13\x0F\x11\x0E\x04\x06\x01\x09\x07\x00\x05\x08\x03\x02\x11\x0D\x0E\x13\x0A\x0B\x0C\x10\x0F\x12\x04\x05\x06\x01\x02\x08\x07\x00\x09\x03\x11\x0E\x0C\x0B\x10\x0D\x0F\x12\x0A\x13\x00\x02\x03\x04\x08\x06\x01\x07\x09\x05\x0A\x13\x12\x0C\x10\x0F\x0E\x0D\x11\x0B\x03\x04\x05\x00\x01\x07\x06\x09\x08\x02\x10\x0D\x0F\x0E\x13\x0C\x11\x0A\x12\x0B\x07\x02\x01\x04\x00\x05\x03\x09\x06\x08\x13\x12\x0C\x10\x0F\x0E\x0D\x11\x0B\x0A\x09\x08\x02\x06\x05\x04\x03\x07\x01\x00\x12\x0E\x0F\x0D\x13\x11\x0B\x10\x0C\x0A\x06\x08\x04\x00\x02\x01\x09\x05\x07\x03\x0C\x0B\x0E\x0D\x11\x12\x13\x0A\x0F\x10\x08\x07\x01\x05\x04\x03\x02\x06\x00\x09\x13\x11\x12\x0C\x0E\x0A\x10\x0B\x0F\x0D\x08\x05\x07\x02\x00\x01\x06\x04\x09\x03\x0B\x0E\x0D\x11\x12\x13\x0A\x0F\x10\x0C\x06\x00\x09\x02\x01\x07\x08\x03\x04\x05\x12\x11\x13\x0C\x0A\x0F\x0D\x0E\x10\x0B\x04\x06\x02\x08\x03\x07\x05\x00\x01\x09\x0F\x12\x13\x0A\x0B\x0C\x10\x11\x0D\x0E\x05\x09\x08\x01\x00\x06\x07\x02\x03\x04\x0A\x0B\x0E\x12\x10\x0D\x0F\x11\x13\x0C\x03\x08\x02\x05\x06\x04\x07\x00\x09\x01\x12\x13\x0A\x0B\x0C\x10\x11\x0D\x0E\x0F\x02\x03\x04\x07\x08\x09\x05\x06\x00\x01\x0A\x0C\x0E\x10\x11\x13\x12\x0D\x0F\x0B\x09\x00\x08\x04\x01\x05\x03\x06\x02\x07\x0B\x0C\x10\x0F\x0E\x0D\x0A\x13\x12\x11\x01\x02\x03\x06\x07\x08\x04\x05\x09\x00\x0F\x12\x0B\x0D\x13\x11\x0A\x0C\x0E\x10\x09\x08\x00\x06\x01\x07\x02\x03\x05\x04\x0C\x10\x0F\x0E\x0D\x0A\x13\x12\x11\x0B\x05\x09\x00\x04\x03\x02\x01\x08\x07\x06\x0E\x12\x0D\x0A\x0C\x0B\x13\x11\x0F\x10\x05\x01\x06\x02\x07\x09\x08\x04\x00\x03\x0E\x0D\x11\x12\x13\x10\x0C\x0B\x0A\x0F\x04\x08\x09\x03\x02\x01\x00\x07\x06\x05\x0B\x0F\x13\x0C\x0A\x10\x0E\x12\x0D\x11\x02\x04\x03\x01\x09\x07\x08\x06\x00\x05\x0D\x11\x12\x13\x10\x0C\x0B\x0A\x0F\x0E\x07\x06\x05\x00\x09\x08\x04\x01\x02\x03\x0B\x13\x10\x0E\x0F\x0C\x0A\x11\x0D\x12\x03\x09\x02\x07\x05\x00\x04\x08\x06\x01\x13\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x06\x05\x04\x09\x08\x07\x03\x00\x01\x02\x11\x0C\x0E\x10\x0D\x12\x0B\x13\x0A\x0F\x05\x09\x04\x07\x02\x03\x01\x06\x08\x00");
  const __FlashStringHelper *UHRBotTop = F("\x0A\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0C\x0E\x10\x0D\x12\x0B\x13\x0A\x0F\x11\x06\x08\x00\x01\x04\x09\x05\x03\x02\x07\x0B\x0C\x0D\x0E\x0F\x10\x11\x12\x13\x0A\x07\x08\x09\x06\x02\x01\x00\x05\x04\x03\x0D\x12\x11\x0E\x10\x0A\x13\x0B\x0F\x0C\x09\x04\x06\x02\x00\x05\x07\x08\x01\x03\x0F\x0E\x13\x12\x11\x0D\x0A\x0B\x0C\x10\x06\x07\x08\x05\x01\x00\x09\x04\x03\x02\x12\x0D\x13\x0B\x0F\x0C\x10\x11\x0A\x0E\x02\x01\x06\x04\x09\x07\x05\x08\x03\x00\x0E\x13\x12\x11\x0D\x0A\x0B\x0C\x10\x0F\x04\x03\x02\x09\x08\x07\x06\x00\x01\x05\x0B\x0A\x12\x0D\x10\x0C\x0F\x11\x0E\x13\x07\x00\x02\x06\x05\x03\x08\x04\x09\x01\x12\x0C\x0B\x0A\x13\x10\x0F\x0E\x0D\x11\x03\x02\x01\x08\x07\x06\x05\x09\x00\x04\x13\x0C\x0F\x11\x0E\x0D\x0A\x10\x12\x0B\x07\x02\x09\x04\x05\x00\x01\x06\x08\x03\x0C\x0B\x0A\x13\x10\x0F\x0E\x0D\x11\x12\x09\x00\x04\x05\x01\x02\x03\x06\x07\x08\x10\x0B\x13\x0D\x0E\x11\x0A\x12\x0C\x0F\x03\x08\x00\x07\x01\x09\x06\x02\x04\x05\x0E\x0F\x10\x0C\x0D\x11\x12\x13\x0A\x0B\x08\x09\x03\x04\x00\x01\x02\x05\x06\x07\x0E\x11\x0A\x12\x13\x10\x0D\x0B\x0F\x0C\x05\x07\x02\x00\x08\x01\x06\x09\x04\x03\x0F\x10\x0C\x0D\x11\x12\x13\x0A\x0B\x0E\x05\x06\x07\x02\x03\x09\x08\x01\x00\x04\x0F\x0C\x13\x12\x0A\x10\x0B\x0E\x11\x0D\x01\x03\x04\x05\x09\x07\x02\x08\x00\x06\x11\x0D\x0E\x13\x0A\x0B\x0C\x10\x0F\x12\x04\x05\x06\x01\x02\x08\x07\x00\x09\x03\x0A\x10\x0B\x0C\x0D\x13\x0F\x11\x0E\x12\x08\x03\x02\x05\x01\x06\x04\x00\x07\x09\x0D\x0E\x13\x0A\x0B\x0C\x10\x0F\x12\x11\x00\x09\x03\x07\x06\x05\x04\x08\x02\x01\x0E\x0C\x0B\x10\x0D\x0F\x12\x0A\x13\x11\x07\x09\x05\x01\x03\x02\x00\x06\x08\x04\x13\x12\x0C\x10\x0F\x0E\x0D\x11\x0B\x0A\x09\x08\x02\x06\x05\x04\x03\x07\x01\x00\x0D\x0F\x0E\x13\x0C\x11\x0A\x12\x0B\x10\x09\x06\x08\x03\x01\x02\x07\x05\x00\x04\x12\x0C\x10\x0F\x0E\x0D\x11\x0B\x0A\x13\x07\x01\x00\x03\x02\x08\x09\x04\x05\x06\x0E\x0F\x0D\x13\x11\x0B\x10\x0C\x0A\x12\x05\x07\x03\x09\x04\x08\x06\x01\x02\x00\x0B\x0E\x0D\x11\x12\x13\x0A\x0F\x10\x0C\x06\x00\x09\x02\x01\x07\x08\x03\x04\x05\x11\x12\x0C\x0E\x0A\x10\x0B\x0F\x0D\x13\x04\x09\x03\x06\x07\x05\x08\x01\x00\x02\x0E\x0D\x11\x12\x13\x0A\x0F\x10\x0C\x0B\x03\x04\x05\x08\x09\x00\x06\x07\x01\x02\x11\x13\x0C\x0A\x0F\x0D\x0E\x10\x0B\x12\x00\x01\x09\x05\x02\x06\x04\x07\x03\x08\x12\x13\x0A\x0B\x0C\x10\x11\x0D\x0E\x0F\x02\x03\x04\x07\x08\x09\x05\x06\x00\x01\x0B\x0E\x12\x10\x0D\x0F\x11\x13\x0C\x0A\x00\x09\x01\x07\x02\x08\x03\x04\x06\x05\x13\x0A\x0B\x0C\x10\x11\x0D\x0E\x0F\x12\x06\x00\x01\x05\x04\x03\x02\x09\x08\x07\x0C\x0E\x10\x11\x13\x12\x0D\x0F\x0B\x0A\x06\x02\x07\x03\x08\x00\x09\x05\x01\x04\x0C\x10\x0F\x0E\x0D\x0A\x13\x12\x11\x0B\x05\x09\x00\x04\x03\x02\x01\x08\x07\x06\x12\x0B\x0D\x13\x11\x0A\x0C\x0E\x10\x0F\x03\x05\x04\x02\x00\x08\x09\x07\x01\x06\x10\x0F\x0E\x0D\x0A\x13\x12\x11\x0B\x0C\x08\x07\x06\x01\x00\x09\x05\x02\x03\x04\x12\x0D\x0A\x0C\x0B\x13\x11\x0F\x10\x0E\x04\x00\x03\x08\x06\x01\x05\x09\x07\x02\x0D\x11\x12\x13\x10\x0C\x0B\x0A\x0F\x0E\x07\x06\x05\x00\x09\x08\x04\x01\x02\x03\x0F\x13\x0C\x0A\x10\x0E\x12\x0D\x11\x0B\x06\x00\x05\x08\x03\x04\x02\x07\x09\x01\x11\x12\x13\x10\x0C\x0B\x0A\x0F\x0E\x0D\x01\x02\x03\x04\x05\x06\x07\x08\x09\x00\x13\x10\x0E\x0F\x0C\x0A\x11\x0D\x12\x0B\x08\x06\x01\x04\x02\x09\x03\x00\x05\x07");

  const __FlashStringHelper *SelUHR;
  byte v;

  // this logic is shorter and faster than eliminating SelUHR and doing two pgm_read_byte each one with a harcoded UHRTopBot/BotTop
  if (TopBot == 0)
  {
    SelUHR = UHRTopBot;
  }
  else
  {
    SelUHR = UHRBotTop;
  }

  //int offset = (((uhr << 1) + (uhr << 3)) << 1) + plug; // (uhr*2+uhr*8)*2 == (uhr*10)*2 (6852 bytes 8472us)
  int offset = uhr * 20 + plug; //fastest & shortest (6830 bytes 8420us)

  v = pgm_read_byte((const char *)SelUHR + offset);
  return v;
}


//BUG enigma to AAAD, hold P and change uhr from 6 to 7 lamps show (B to Z), other letters also light briefly
//
void doUhr()     /*xls*/
{
  //plugA = 0..9
  //plugB = 10..19

  static byte state = 0;
  static byte uhr = 0;
  static byte pluga = 0;
  static byte plugb = 255;
  static int  etimer = 0;
  static byte nextstate = 0;

  byte plugav;

  if (state == 0)
  {
    pluga++;

    if (pluga == 20)
    {
      pluga = 0;
    }
  }

  plugav = readPlug(pluga);

  // return to state 0 logic
  if ((state != 0) && (plugav != 0))
  {
    if (plugb != 255)
    {
      writePlug(plugb, 1);
    }

    plugb = 255;
    state = 0;
  }

  switch (state)
  {
    case 0:
      {
        if (plugav == 0)
        {
          uhr = UHRData.UHR; // if uhr has changed, update now, prevent glitches if uhr is changed mid state
          plugb = 255;
          state = 1;
        }
        break;
      }

    case 1:
      {
        plugb = GetPlug(uhr, pluga, 0); //plugb = uhrtabletopbot[pluga];
        writePlug(plugb, 0);

        etimer = etimervalue;  // tune this value 750
        nextstate = 2;
        state = 99;
        break;
      }

    case 2:
      {
        writePlug(plugb, 1);

        state = 3;
        break;
      }

    case 3:
      {
        plugb = GetPlug(uhr, pluga, 1); //plugb = uhrtablebottop[pluga];
        writePlug(plugb, 0);

        etimer = etimervalue;
        nextstate = 4;
        state = 99;
      }

    case 4:
      {
        // stay here until logic to return to state 0 kicks in

        break;
      }

    case 99:
      {
        etimer--;
        if (etimer == 0)
        {
          state = nextstate;
        }

        break;
      }
  }
}
