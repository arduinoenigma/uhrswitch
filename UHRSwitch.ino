// UHRSwitch.ino
// @arduinoenigma 2020

#include "GPIO.h"
#include <EEPROM.h>

#define CommonCathode
//#define CommonAnode


//
void setup()      /*xls*/
{
  // put your setup code here, to run once:

  Serial.begin(9600);
  //Serial.begin(250000);

  Serial.print(F("UhrSwitch 20200614\x0d\x0a"));

  allPlugsInput();
  initEncoderPins();

  allSegmentOff();
  allSegmentOutput();

  allDigitsOff();
  allDigitsOutput();

  initLamp();

  //TestPrintAllUhr(); //BUG: disable
  //TestPrintUHRPairs(0); //BUG: disable
  //TestPrintAllUhr2(); //BUG: disable

  //TestPrintHex(); //BUG: disable
  //GenerateTables(); //BUG: disable
  //TestGetPlug(); //BUG: disable
}


//
void loop()      /*xls*/
{
  // put your main code here, to run repeatedly:

  static byte uhr = 0;
  signed char v;

  v = rotaryencoder();
  uhr += v;

  if (uhr == 255) // ask this first since 255 is >40
  {
    uhr = 39;
  }

  if (uhr > 39)
  {
    uhr = 0;
  }

  SetUHR(uhr);
  Display(uhr);
  doUhr();

  //flashD13();

  //if (v != 0)
  //{
  //allPlugsInput();
  //Serial.print(F("UHR:"));
  //Serial.println(uhr);
  //enableAllPlugs(uhr);
  //}
}
