// UHRSwitch.ino - Display.ino
// @arduinoenigma 2020

const unsigned long leftdigit[10] PROGMEM = {RawBits0L, RawBits1L, RawBits2L, RawBits3L, RawBits4L, RawBits5L, RawBits6L, RawBits7L, RawBits8L, RawBits9L};
const unsigned long rightdigit[10] PROGMEM = {RawBits0R, RawBits1R, RawBits2R, RawBits3R, RawBits4R, RawBits5R, RawBits6R, RawBits7R, RawBits8R, RawBits9R};


//
void selectDigit(byte digit)      /*xls*/
{
  allDigitsOff();

  switch (digit) {
    case 0:
      Digit1.DigitOn();
      break;
    case 1:
      Digit2.DigitOn();
      break;
  }
}


//
void displayRaw(unsigned long raw, byte digit)      /*xls*/
{
  unsigned long mask;

  allSegmentOff();

  if (digit == 0)
  {
    mask = MaskL;
  }
  else
  {
    mask = MaskR;
  }

  if (raw & (MaskAR | MaskAL) & mask)
    //if (raw & ( ((digit == 0) && MaskAL) || ((digit == 1) && MaskAR) ) )
  {
    SegmentA.SegmentOn();
  }
  else
  {
    SegmentA.SegmentOff();
  }

  //if (raw & ( ((digit == 0) && MaskBL) || ((digit == 1) && MaskBR) ) )
  if (raw & (MaskBR | MaskBL) & mask)
  {
    SegmentB.SegmentOn();
  }
  else
  {
    SegmentB.SegmentOff();
  }

  if (raw & (MaskCR | MaskCL) & mask)
  {
    SegmentC.SegmentOn();
  }
  else
  {
    SegmentC.SegmentOff();
  }

  if (raw & (MaskDR | MaskDL) & mask)
  {
    SegmentD.SegmentOn();
  }
  else
  {
    SegmentD.SegmentOff();
  }

  if (raw & (MaskER | MaskEL) & mask)
  {
    SegmentE.SegmentOn();
  }
  else
  {
    SegmentE.SegmentOff();
  }

  if (raw & (MaskFR | MaskFL) & mask)
  {
    SegmentF.SegmentOn();
  }
  else
  {
    SegmentF.SegmentOff();
  }

  if (raw & (MaskGR | MaskGL) & mask)
  {
    SegmentG.SegmentOn();
  }
  else
  {
    SegmentG.SegmentOff();
  }
}


//
void Display(byte newuhr)      /*xls*/
{
  // current value to display
  //static byte DisplayValues[2] = {1, 2};
  // the next value to display
  //static byte NewDisplayValues[2] = {1, 3};

  static byte uhr = 0;

  static byte state = 0;
  static byte digit = 0;
  static unsigned long timer = 0;

  static unsigned long NewRawDisplayValues = 0;
  static unsigned long RawDisplayValues = RawBits0L | RawBits0R;

  static byte blanksteps = 0;

  static byte scrollsteps = 0;
  static int scrolltimer = 0;

  /*
    if (scrollsteps) // segment level scrolling
    {
    scrolltimer++;
    if (scrolltimer == 500)
    {
      scrolltimer = 0;
      scrollsteps--;

      //RawDisplayValues /= 2;
      //RawDisplayValues &= MaskSL;

      RawDisplayValues *= 2;
      RawDisplayValues &= MaskSR;

      if (scrollsteps < 4)
      {
        unsigned long D2 = NewRawDisplayValues;
        for (byte i = 0; i < scrollsteps; i++)
        {
          D2 /= 2;
          D2 &= MaskSL;
        }
        RawDisplayValues |= D2;
      }

      if (scrollsteps == 0)
      {
        RawDisplayValues = NewRawDisplayValues;
      }
    }
    }
  */

  /*
    if (blanksteps) // blink digits one at a time
    {
    scrolltimer++;
    if (scrolltimer == 2500)
    {
      scrolltimer = 0;
      blanksteps--;

      switch (blanksteps)
      {
        case 3:
          {
            RawDisplayValues = RawDisplayValues & MaskR;
            //RawDisplayValues = 0;
            break;
          }
        case 2:
          {
            RawDisplayValues = 0;
            //RawDisplayValues = NewRawDisplayValues;
            break;
          }
        case 1:
          {
            RawDisplayValues = NewRawDisplayValues & MaskL;
            //RawDisplayValues = NewRawDisplayValues;
            break;
          }
        case 0:
          {
            RawDisplayValues = NewRawDisplayValues;
            break;
          }
      }
    }
    }
  */


  /*
    if (blanksteps)  // blink
    {
    scrolltimer++;
    if (scrolltimer == 2500)
    {
      scrolltimer = 0;
      blanksteps--;

      switch (blanksteps)
      {
        case 3:
          {
            RawDisplayValues = 0;
            break;
          }
        case 2:
          {
            RawDisplayValues = NewRawDisplayValues;
            break;
          }
        case 1:
          {
            //RawDisplayValues = NewRawDisplayValues & MaskL;
            //RawDisplayValues = NewRawDisplayValues;
            break;
          }
        case 0:
          {
            //RawDisplayValues = NewRawDisplayValues;
            break;
          }
      }
    }
    }
  */

  if (newuhr != uhr)
  {
    uhr = newuhr;
    byte l, r;

    l = uhr / 10;
    r = uhr - l * 10;

    NewRawDisplayValues = pgm_read_dword(leftdigit + l);
    NewRawDisplayValues |= pgm_read_dword(rightdigit + r);

    scrolltimer = 0;

    blanksteps = 4;
    //scrollsteps = 6;

    RawDisplayValues = NewRawDisplayValues; // uncomment to skip effects
  }

  switch (state)
  {
    case 0:
      {
        displayRaw(RawDisplayValues, digit);
        selectDigit(digit);
        timer = micros();
        state = 1;
        break;
      }
    case 1:
      {
        if ((micros() - timer) > 20)
        {
          selectDigit(99);
          timer = micros();
          state = 2;
        }
        break;
      }
    case 2:
      {
        if ((micros() - timer) > 370)
        {
          digit++;
          if (digit == 2)
          {
            digit = 0;
          }
          state = 0;
        }
        break;
      }
  }
}
