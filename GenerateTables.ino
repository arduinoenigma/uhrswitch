// UHRSwitch.ino - GenerateTables.ino
// @arduinoenigma 2020

// The functions in this file are used to generate the tables *UHRTopBot and *UHRBotTop in UhrSwitchLogic.ino:GetPlug()
// These functions take a non trivial amount of time to execute and should not be called during normal use if GetPlug() is used

byte uhrtabletopbot[20];
byte uhrtablebottop[20];


// plugA = 0..9
// plugB = 10..19
byte getInverse(byte plug, byte uhr)     /*xls*/
{
  // Left to Right UHR disk is 0 based, contact 0 goes to contact 6 on the other side (taken from Palloks Universal Enigma and converted to hex)
  const __FlashStringHelper *UHRABSF = F("\x06\x1F\x04\x1D\x12\x27\x10\x19\x1E\x17\x1C\x01\x26\x0B\x24\x25\x1A\x1B\x18\x15\x0E\x03\x0C\x11\x02\x07\x00\x21\x0A\x23\x08\x05\x16\x13\x14\x0D\x22\x0F\x20\x09");

  // Right to Left UHR disk is 0 based, contact 0 goes to contact 6 on the other side (taken from Palloks Universal Enigma and converted to hex)
  const __FlashStringHelper *UHRBASF = F("\x1A\x0B\x18\x15\x02\x1F\x00\x19\x1E\x27\x1C\x0D\x16\x23\x14\x25\x06\x17\x04\x21\x22\x13\x20\x09\x12\x07\x10\x11\x0A\x03\x08\x01\x26\x1B\x24\x1D\x0E\x0F\x0C\x05");

  // A to B plugs is 0 based, plug 0 (1A) is opposite of plug 6 (7B) (taken from Palloks Universal Enigma and converted to hex)
  const __FlashStringHelper *UHRPLUGABSF =  F("\x06\x00\x07\x05\x01\x08\x04\x02\x09\x03");

  // B to A plugs is 0 based, translates B plugs to an index in UHRBASF (plug 1b connects to pin UHRBASF[0x4] (0x02), plug 1b connects to UHRBASF[0x10] (own work @arduinoenigma)
  const __FlashStringHelper *UHRPLUGBASF =  F("\x04\x10\x1C\x24\x18\x0C\x00\x08\x14\x20");

  signed char ndx;
  signed char k;

  if (plug < 10)
  {
    ndx = uhr + (plug << 2); // same as plug*4
    if (ndx > 39)
    {
      ndx -= 40;
    }

    k = (pgm_read_byte((const char *)UHRABSF + ndx) - uhr) - 2;
    if (k < 0)
    {
      k += 40;
    }

    k = k >> 2; // k/4

    k = pgm_read_byte((const char *)UHRPLUGABSF + k) + 10; // add 10 to turn into b plug
  }
  else
  {
    ndx = uhr + pgm_read_byte((const char *)UHRPLUGBASF + (plug - 10)); // sub 10 to turn b plug into index
    if (ndx > 39)
    {
      ndx -= 40;
    }

    k = (pgm_read_byte((const char *)UHRBASF + ndx) - uhr) - 2;

    if (k < 0)
    {
      k += 40;
    }

    k = k >> 2;
  }

  return k;
}


//
// This function used to be called when rotating the encoder to compute the current A->B and B->A tables
//
void enableAllPlugs(byte currentuhr)     /*xls*/
{
  static byte uhr = 254;
  static byte newuhr = 254;

  if (currentuhr != 255)
  {
    uhr = currentuhr;
  }
  else
  {
    if (uhr != newuhr)
    {
      newuhr = uhr;

      selectDigit(99);
      allPlugsInput();

      for (byte i = 0; i < 20; i++)
      {
        byte invi = getInverse(i, uhr);
        uhrtabletopbot[i] = invi;
        uhrtablebottop[invi] = i;
      }

      //printtables(); //BUG: disable when done
    }
  }
}


// d goes from 0..19
void PrintHex(byte d)      /*xls*/
{
  const __FlashStringHelper *HEXT = F("000102030405060708090A0B0C0D0E0F1011121314");

  Serial.print(F("\\x"));
  Serial.print((char)pgm_read_byte((const char *)HEXT + (d * 2)));
  Serial.print((char)pgm_read_byte((const char *)HEXT + (d * 2) + 1));
}


// This function generates the two tables used in UhrSwitchLogic.ino:GetPlug()
//
void GenerateTables()      /*xls*/
{
  for (byte i = 0; i < 2; i++)
  {
    if (i == 0)
    {
      Serial.print(F("const __FlashStringHelper *UHRTopBot = F(\""));
    }
    else
    {
      Serial.print(F("const __FlashStringHelper *UHRBotTop = F(\""));
    }

    for (byte j = 0; j < 40; j++)
    {
      enableAllPlugs(j);
      enableAllPlugs(255);

      byte v;

      for (byte k = 0; k < 20; k++)
      {
        if (i == 0)
        {
          v = uhrtabletopbot[k];
        }
        else
        {
          v = uhrtablebottop[k];
        }

        PrintHex(v);

        //Serial.print(v);
        //Serial.print(' ');
      }
    }
    Serial.println("\");");
  }
}


// This function compares the values returned by enableAllPlugs() and UhrSwitchLogic.ino:GetPlug()
// it is used to verify the GetPlug() implementation is correc
//
void TestGetPlug()      /*xls*/
{
  byte v1;
  byte v2;

  Serial.print(F("Testing GetPlug():"));

  for (byte i = 0; i < 40; i++)
  {
    enableAllPlugs(i);
    enableAllPlugs(255);

    for (byte j = 0; j < 20; j++)
    {
      for (byte k = 0; k < 2; k++)
      {
        if (k == 0)
        {
          v1 = uhrtabletopbot[j];
        }
        else
        {
          v1 = uhrtablebottop[j];
        }

        v2 = GetPlug(i, j, k);

        if (v1 != v2)
        {
          Serial.print(i);
          Serial.print(':');
          Serial.print(j);
          Serial.print(':');
          Serial.print(k);
          Serial.print(':');
          Serial.print(v1);
          Serial.print(' ');
          Serial.println(v2);
        }
      }
    }
  }

  Serial.print(F(" done\x0d\x0a"));
  Serial.print(micros());
  Serial.print(F("uS\x0d\x0a"));
}


//
void TestPrintHex()      /*xls*/
{
  for (byte i = 0; i < 21; i++)
  {
    Serial.print(i);
    Serial.print(':');
    PrintHex(i);
    Serial.print(':');
  }
}


//
void printtables()     /*xls*/
{
  Serial.print(F("t\x0d\x0a"));

  for (byte i = 0; i < 20; i++)
  {
    Serial.print(i + 1);
    Serial.print(' ');
    Serial.print(uhrtabletopbot[i] + 1);
    Serial.print(F("\x0d\x0a"));
  }

  Serial.print(F("b\x0d\x0a"));

  for (byte i = 0; i < 20; i++)
  {
    Serial.print(i + 1);
    Serial.print(' ');
    Serial.print(uhrtablebottop[i] + 1);
    Serial.print(F("\x0d\x0a"));
  }
}


//
void TestPrintUHRPairs(byte UHR)    /*xls*/
{
  const __FlashStringHelper *UHRSF = F("\x06\x1F\x04\x1D\x12\x27\x10\x19\x1E\x17\x1C\x01\x26\x0B\x24\x25\x1A\x1B\x18\x15\x0E\x03\x0C\x11\x02\x07\x00\x21\x0A\x23\x08\x05\x16\x13\x14\x0D\x22\x0F\x20\x09");
  const __FlashStringHelper *UHRPLUGSF =  F("\x06\x00\x07\x05\x01\x08\x04\x02\x09\x03");

  const char *uhrptr = (const char *)UHRSF;
  const char *uhrplugptr = (const char *)UHRPLUGSF;

  Serial.print(F("UHR: "));
  Serial.println(UHR);

  if (true)
  {
    for (byte i = 0; i < 26; i++)
    {
      //UnivEnigmaWheels.EffectivePlugs[i] = i + 1;
    }

    for (byte i = 0; i < 10; i++)
    {
      byte pin = 0;
      byte pinright = 0;
      byte pinleft = 0;

      pin = UHR + i * 4;
      if (pin > 39)
      {
        pin -= 40;
      }

      for (byte j = 0; j < 40; j++)
      {
        if (pgm_read_byte(uhrptr + j) == pin)
        {
          pinleft = j;
        }
      }

      pinright = pgm_read_byte(uhrptr + pin);

      //these two need to be signed, see <0 below
      //char is signed -127..+128
      char plugright;
      char plugleft;

      plugright = (pinright - (UHR + 2));
      if (plugright < 0)
      {
        plugright += 40;
      }
      plugright = plugright >> 2; //same as / 4;

      plugleft = (pinleft - (UHR + 2));
      if (plugleft < 0)
      {
        plugleft += 40;
      }
      plugleft = plugleft >> 2; //same as / 4;

      //EffSTECKER[EnigmaData.PAIRS[i * 2] - 65] = EnigmaData.PAIRS[pgm_read_byte(uhrplugptr + plugright) * 2 + 1];
      //EffSTECKER[EnigmaData.PAIRS[pgm_read_byte(uhrplugptr + i) * 2 + 1] - 65] = EnigmaData.PAIRS[plugleft * 2];

      Serial.print(i + 1);
      Serial.print(F("a->"));
      Serial.print(pgm_read_byte(uhrplugptr + plugright) + 1);

      Serial.print(F("b "));
      Serial.print(pgm_read_byte(uhrplugptr + i) + 1);
      Serial.print(F("b->"));
      Serial.print((byte)(plugleft + 1));

      Serial.println(F("a"));

      //arrays are zero based (A==0), values inside are 1 based (A==1)
      //UnivEnigmaWheels.EffectivePlugs[GetPlugPair(i, 0) - 1] = GetPlugPair(pgm_read_byte(uhrplugptr + plugright), 1);
      //UnivEnigmaWheels.EffectivePlugs[GetPlugPair(pgm_read_byte(uhrplugptr + i), 1) - 1] = GetPlugPair(plugleft, 0);
    }
  }
}


//
void TestPrintAllUhr()    /*xls*/
{
  for (byte i = 0; i < 40; i++)
  {
    TestPrintUHRPairs(i);
  }

  do
  {

  } while (1);
}


//
void TestPrintAllUhr2()      /*xls*/
{
  char uhr[] = "abcdefghijklmnopqrstuvwxyz";

  Serial.print(F("each string below should match the ones at\x0d\x0a"));
  Serial.print(F("https://arduinoenigma.blogspot.com/2020/03/enigma-uhr-switch-test-vectors.html\x0d\x0a"));

  for (byte i = 0; i < 40; i++)
  {
    Serial.print(F("UHR: "));
    Serial.print(i);
    Serial.print(F("\x0d\x0a"));

    for (byte j = 0; j < 20; j++)
    {
      byte v = getInverse(j, i);
      byte u;
      byte n;

      if (j < 10)
      {
        n = j << 1;
      }
      else
      {
        n = ((j - 10) << (byte)1) + 1;
      }

      if (v < 10)
      {
        u = v << 1;
      }
      else
      {
        u = ((v - 10) << 1) + 1;
      }

      //Serial.print(i);
      //Serial.print(' ');
      //Serial.print(j);
      //Serial.print(' ');
      //Serial.print(v);
      //Serial.print(' ');
      //Serial.print((char)('a' + u));
      //Serial.print(F("\x0d\x0a"));

      uhr[n] = 'a' + u;
    }

    for (byte j = 0; j < 26; j++)
    {
      Serial.print(uhr[j]);
    }
    Serial.print(F("\x0d\x0a"));
  }

  do
  {

  } while (1);
}
