// UHRSwitch.ino - DeviceIO.ino
// @arduinoenigma 2020

#if defined(CommonCathode)
#define SegmentOn high
#define SegmentOff low
#define DigitOn low
#define DigitOff high
#define LampsOn low
#define LampsOff high
#endif

#if defined(CommonAnode)
#define SegmentOn low
#define SegmentOff high
#define DigitOn high
#define DigitOff low
#define LampsOn low
#define LampsOff high
#endif

//
// A0 = D14
// A1 = D15
// A2 = D16
// A3 = D17
// A4 = D18
// A5 = D19
// A6 = D20
// A7 = D21
//

GPIO<BOARD::D10> EncBp;
GPIO<BOARD::D12> EncAp;
GPIO<BOARD::D14> EncGnd;

GPIO<BOARD::D8>  EncPb;
GPIO<BOARD::D6>  EncPbGnd;

GPIO<BOARD::D13> LAMP;

GPIO<BOARD::D50> SegmentA;
GPIO<BOARD::D52> SegmentB;
GPIO<BOARD::D44> SegmentC;
GPIO<BOARD::D42> SegmentD;
GPIO<BOARD::D40> SegmentE;
GPIO<BOARD::D48> SegmentF;
GPIO<BOARD::D47> SegmentG;
GPIO<BOARD::D46> SegmentDP;
GPIO<BOARD::D38> Digit1;
GPIO<BOARD::D36> Digit2;


//
void allSegmentOff()      /*xls*/
{
  SegmentA.SegmentOff();
  SegmentB.SegmentOff();
  SegmentC.SegmentOff();
  SegmentD.SegmentOff();
  SegmentE.SegmentOff();
  SegmentF.SegmentOff();
  SegmentG.SegmentOff();
  SegmentDP.SegmentOff();
}


//
void allSegmentOutput()      /*xls*/
{
  SegmentA.output();
  SegmentB.output();
  SegmentC.output();
  SegmentD.output();
  SegmentE.output();
  SegmentF.output();
  SegmentG.output();
  SegmentDP.output();
}


//
void allDigitsOff()      /*xls*/
{
  Digit1.DigitOff();
  Digit2.DigitOff();
}


//
void allDigitsOutput()      /*xls*/
{
  Digit1.output();
  Digit2.output();
}


//
void initEncoderPins()      /*xls*/
{
  EncAp.input();
  EncBp.input();
  EncPb.input();
  EncGnd.input();

  EncAp.high();
  EncBp.high();
  EncPb.high();
  EncGnd.low();

  EncGnd.output();
}


//
void initLamp()      /*xls*/
{
  LAMP.low();
  LAMP.output();
}


//
void flashD13()     /*xls*/
{
  static int lamptimer = 0;
  lamptimer++;

  if (lamptimer > 256)
  {
    LAMP.low();
  }
  else
  {
    LAMP.high();
  }
}


//
void showrawenc()     /*xls*/
{
  byte EncA = EncAp.read();
  byte EncB = EncBp.read();
  static byte changing = 0;

  if (EncA + EncB != 2)
  {
    Serial.print(EncA);
    Serial.println(EncB);
    changing = 1;
  }
  else
  {
    if (changing == 1)
    {
      Serial.println(F("done"));
      changing = 0;
    }
  }
}


//debounced rotary encoder logic
signed char rotaryencoder()     /*xls*/
{
  static byte state = 0;
  static signed char dir = 0;
  signed char v = 0;

  //read pins one at a time
  //byte EncA = EncAp.read(); //EncA must be input() and high()
  //byte EncB = EncBp.read(); //EncB must be input() and high(), connect C to ground

  byte ENC = PINB;                      //read both pins at a time
  byte EncA = (ENC & 0b01000000) >> 6;  // isolate bit (&) 0x08 = D11 and shift right to turn into 1 or 0
  byte EncB = (ENC & 0b00010000) >> 4;

  //Serial.println(EncB);

  switch (state)
  {
    case 0:
      {
        if (EncA + EncB != 2)  // at rest encoder outputs 11
        {
          if (EncA == 1)       // followed by many 10 when turning right
          {
            dir = -1;
            state = 1;         // detect the rising edge and then wait until it goes 00 then back to 11
          }

          if (EncB == 1)       // followed by many 01 when turning left
          {
            dir = 1;
            state = 1;
          }                    // if output is 00, we missed 01 or 00, stay in this state
        }

        break;
      }
    case 1:
      {
        // once we react to the first 01 or 10, many more, followed by 00 will follow

        if (EncA + EncB == 0) // after initial 10 or 01, we must have a 00 to confirm it was valid
        {
          v = dir;
          dir = 0;
        }

        if (EncA + EncB == 2) // once we see 11 again, the encoder is idle, go back to detecting a change
        {
          state = 0;
        }
      }
  }

  return v;
}
